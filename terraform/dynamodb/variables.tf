variable "table_name" {
  type        = string
  description = "DynamoDB table name."
}

variable "billing_mode" {
  type        = string
  description = "DynamoDB billing mode."
}

variable "read_capacity" {
  type        = number
  description = "Read Capacity billing mode."
  default     = null
}

variable "write_capacity" {
  type        = number
  description = "Write Capacity billing mode."
  default     = null
}

variable "hash_key" {
  type        = string
  description = "Primary key (hash_key) of the table."
}

variable "range_key" {
  type        = string
  description = "Secondary key (range_key) of the table."
  default     = null
}

variable "attributes" {
  type = set(object({
    name = string
    type = string
  }))
  description = "Definition of DynamoDB attributes."
}

variable "global_secondary_index" {
  type = object({
    name               = string
    hash_key           = string
    range_key          = optional(string)
    write_capacity     = optional(number)
    read_capacity      = optional(number)
    projection_type    = string
    non_key_attributes = optional(set(string))
  })
}
