resource "aws_organizations_policy" "this" {
  name    = var.scp_name != null ? var.scp_name : trimsuffix(var.json_file, ".json")
  content = file("${var.path}/${var.json_file}")
}

resource "aws_organizations_policy_attachment" "this" {
  for_each = var.ou_list

  target_id = each.value
  policy_id = aws_organizations_policy.this.id
}
