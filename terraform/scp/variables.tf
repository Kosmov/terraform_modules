variable "ou_list" {
  description = "list of management OUs"
  type        = set(string)
}

variable "json_file" {
  description = "Path to json file for SCP"
  type        = string
}

variable "scp_name" {
  type        = string
  description = "Name of the Service Control Policy"
  default     = null
}

variable "path" {
  type        = string
  description = "Path to the SCP files"
}
