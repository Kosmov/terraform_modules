variable "statements" {
  type = map(object({
    actions   = set(string)
    resources = set(string)
  }))
}

variable "name" {
  type = string
}

variable "path" {
  type = string
}

variable "description" {
  type = string
}
