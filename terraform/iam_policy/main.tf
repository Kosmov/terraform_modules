resource "aws_iam_policy" "this" {
  name = var.name
  path = var.path
  description = var.description
  policy = data.aws_iam_policy_document.this.json
}