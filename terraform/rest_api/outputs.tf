output "execution_arn" {
  value = aws_api_gateway_rest_api.this.execution_arn
}

output "api_endpoint" {
  value = "${aws_api_gateway_deployment.this.invoke_url}${var.stage_name}/<endpoint>"
}

output "invoke_url" {
  value = aws_api_gateway_stage.this.invoke_url
}
