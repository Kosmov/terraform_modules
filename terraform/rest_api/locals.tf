locals {
  uri = "arn:aws:apigateway:${data.aws_region.this.id}:lambda:path/2015-03-31/functions/arn:aws:lambda:${data.aws_region.this.id}:${data.aws_caller_identity.this.id}:function:%s/invocations"
  authorizers = {
    for k, v in var.authorizers : k => {
      identity_source = v.identity_source
      type            = v.type
      provider_arns   = toset(flatten([for id in v.cognito_names : data.aws_cognito_user_pools.this[id].arns]))
    }
  }
}
