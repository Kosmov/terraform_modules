resource "aws_api_gateway_rest_api" "this" {
  name = var.api_name
}

resource "aws_api_gateway_authorizer" "this" {
  depends_on = [data.aws_cognito_user_pools.this]
  for_each   = local.authorizers

  name            = each.key
  rest_api_id     = aws_api_gateway_rest_api.this.id
  identity_source = each.value.identity_source
  type            = each.value.type
  provider_arns   = each.value.provider_arns
}

resource "aws_api_gateway_gateway_response" "unauthorized" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  status_code   = "401"
  response_type = "UNAUTHORIZED"
  response_templates = {
    "application/json" = "{\"message\":$context.error.messageString}"
  }
  response_parameters = {
    "gatewayresponse.header.Access-Control-Allow-Origin" : "'*'"
  }
}

resource "aws_api_gateway_gateway_response" "integration_error" {
  rest_api_id   = aws_api_gateway_rest_api.this.id
  status_code   = "500"
  response_type = "INTEGRATION_FAILURE"
  response_templates = {
    "application/json" = "{\"message\":$context.error.messageString}"
  }
  response_parameters = {
    "gatewayresponse.header.Access-Control-Allow-Origin" : "'*'"
  }
}

resource "aws_api_gateway_deployment" "this" {
  rest_api_id = aws_api_gateway_rest_api.this.id
  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_integration.post,
      aws_api_gateway_method.post,
      aws_api_gateway_integration.get,
      aws_api_gateway_method.get,
      aws_api_gateway_integration.delete,
      aws_api_gateway_method.delete,
      aws_api_gateway_integration.patch,
      aws_api_gateway_method.patch,
      aws_api_gateway_resource.this,
      aws_api_gateway_authorizer.this,
      aws_api_gateway_method.options,
      aws_api_gateway_integration.options
      ])
    )
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "this" {
  deployment_id = aws_api_gateway_deployment.this.id
  rest_api_id   = aws_api_gateway_rest_api.this.id
  stage_name    = var.stage_name
}

resource "aws_api_gateway_resource" "this" {
  for_each = var.resource_paths

  rest_api_id = aws_api_gateway_rest_api.this.id
  parent_id   = aws_api_gateway_rest_api.this.root_resource_id
  path_part   = each.value
}

resource "aws_api_gateway_method" "post" {
  for_each = try(var.post_methods, null)

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this[each.key].id
  http_method   = each.value.http_method
  authorization = each.value.authorization
  authorizer_id = each.value.authorization != "NONE" ? aws_api_gateway_authorizer.this[each.value.authorizer_name].id : null
}

resource "aws_api_gateway_method" "get" {
  for_each = try(var.get_methods, null)

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this[each.key].id
  http_method   = each.value.http_method
  authorization = each.value.authorization
  authorizer_id = each.value.authorization != "NONE" ? aws_api_gateway_authorizer.this[each.value.authorizer_name].id : null
}

resource "aws_api_gateway_method" "options" {
  for_each = try(var.options_methods, null)

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this[each.key].id
  http_method   = each.value.http_method
  authorization = each.value.authorization
  authorizer_id = each.value.authorization != "NONE" ? aws_api_gateway_authorizer.this[each.value.authorizer_name].id : null
}

resource "aws_api_gateway_method" "delete" {
  for_each = try(var.delete_methods, null)

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this[each.key].id
  http_method   = each.value.http_method
  authorization = each.value.authorization
  authorizer_id = each.value.authorization != "NONE" ? aws_api_gateway_authorizer.this[each.value.authorizer_name].id : null
}

resource "aws_api_gateway_method" "patch" {
  for_each = try(var.delete_methods, null)

  rest_api_id   = aws_api_gateway_rest_api.this.id
  resource_id   = aws_api_gateway_resource.this[each.key].id
  http_method   = each.value.http_method
  authorization = each.value.authorization
  authorizer_id = each.value.authorization != "NONE" ? aws_api_gateway_authorizer.this[each.value.authorizer_name].id : null
}

resource "aws_api_gateway_integration" "post" {
  for_each = try(var.post_integrations, null)

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this[each.key].id
  http_method             = aws_api_gateway_method.post[each.key].http_method
  type                    = each.value.type
  uri                     = format(local.uri, each.value.function_name)
  integration_http_method = each.value.integration_http_method
  request_templates       = each.value.request_templates
}

resource "aws_api_gateway_integration" "get" {
  for_each = try(var.get_integrations, null)

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this[each.key].id
  http_method             = aws_api_gateway_method.get[each.key].http_method
  type                    = each.value.type
  uri                     = format(local.uri, each.value.function_name)
  integration_http_method = each.value.integration_http_method
  request_templates       = each.value.request_templates
}

resource "aws_api_gateway_integration" "options" {
  for_each = try(var.options_integrations, null)

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this[each.key].id
  http_method             = aws_api_gateway_method.options[each.key].http_method
  type                    = each.value.type
  integration_http_method = each.value.integration_http_method
  request_templates       = each.value.request_templates

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_integration" "delete" {
  for_each = try(var.delete_integrations, null)

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this[each.key].id
  http_method             = aws_api_gateway_method.delete[each.key].http_method
  type                    = each.value.type
  uri                     = format(local.uri, each.value.function_name)
  integration_http_method = each.value.integration_http_method
  request_templates       = each.value.request_templates
}

resource "aws_api_gateway_integration" "patch" {
  for_each = try(var.patch_integrations, null)

  rest_api_id             = aws_api_gateway_rest_api.this.id
  resource_id             = aws_api_gateway_resource.this[each.key].id
  http_method             = aws_api_gateway_method.patch[each.key].http_method
  type                    = each.value.type
  uri                     = format(local.uri, each.value.function_name)
  integration_http_method = each.value.integration_http_method
  request_templates       = each.value.request_templates
}

resource "aws_api_gateway_method_response" "options" {
  for_each = try(var.options_method_responses, null)

  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_resource.this[each.key].id
  http_method = aws_api_gateway_method.options[each.key].http_method
  status_code = each.value.status_code

  response_parameters = each.value.response_parameters
  response_models     = each.value.response_models
}

resource "aws_api_gateway_integration_response" "options" {
  depends_on = [aws_api_gateway_integration.options]
  for_each   = try(var.options_integration_responses, null)

  rest_api_id = aws_api_gateway_rest_api.this.id
  resource_id = aws_api_gateway_resource.this[each.key].id
  http_method = aws_api_gateway_method.options[each.key].http_method
  status_code = each.value.status_code

  response_parameters = each.value.response_parameters

  lifecycle {
    create_before_destroy = true
  }
}
