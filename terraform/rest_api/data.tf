data "aws_caller_identity" "this" {}
data "aws_region" "this" {}
data "aws_cognito_user_pools" "this" {
  for_each = toset(flatten([
    for authorizer_key, authorizer_value in var.authorizers : flatten(authorizer_value.cognito_names)
  ]))
  name = each.value
}