variable "api_name" {
  type        = string
  description = "Name of the REST API"
}

variable "stage_name" {
  type        = string
  description = "Name of the stage"
}

variable "resource_paths" {
  type        = set(string)
  description = "Resource paths"
}

variable "post_methods" {
  type = map(object({
    http_method     = string
    authorization   = string
    authorizer_name = optional(string)
  }))
  description = "Methods configaration"
  default     = {}
}

variable "get_methods" {
  type = map(object({
    http_method     = string
    authorization   = string
    authorizer_name = optional(string)
  }))
  description = "Methods configaration"
  default     = {}
}

variable "options_methods" {
  type = map(object({
    http_method     = string
    authorization   = string
    authorizer_name = optional(string)
  }))
  description = "Methods configaration"
  default     = {}
}

variable "delete_methods" {
  type = map(object({
    http_method     = string
    authorization   = string
    authorizer_name = optional(string)
  }))
  description = "Methods configaration"
  default     = {}
}

variable "patch_methods" {
  type = map(object({
    http_method     = string
    authorization   = string
    authorizer_name = optional(string)
  }))
  description = "Methods configaration"
  default     = {}
}

variable "post_integrations" {
  type = map(object({
    function_name           = string
    type                    = string
    integration_http_method = string
    request_templates       = optional(map(string))
  }))
  description = "Methods configaration"
  default     = {}
}

variable "get_integrations" {
  type = map(object({
    function_name           = string
    type                    = string
    integration_http_method = string
    request_templates       = optional(map(string))
  }))
  description = "Methods configaration"
  default     = {}
}

variable "options_integrations" {
  type = map(object({
    type                    = string
    integration_http_method = string
    request_templates       = optional(map(string))
  }))
  description = "Methods configaration"
  default     = {}
}

variable "delete_integrations" {
  type = map(object({
    function_name           = string
    type                    = string
    integration_http_method = string
    request_templates       = optional(map(string))
  }))
  description = "Methods configaration"
  default     = {}
}

variable "patch_integrations" {
  type = map(object({
    function_name           = string
    type                    = string
    integration_http_method = string
    request_templates       = optional(map(string))
  }))
  description = "Methods configaration"
  default     = {}
}

variable "authorizers" {
  type = map(object({
    type            = string
    identity_source = string
    cognito_names   = optional(set(string))
    provider_arns   = set(string)
  }))
  default     = {}
  description = "Map of authorizers which authorizes API calls."
}

variable "options_method_responses" {
  type = map(object({
    status_code         = string
    response_parameters = map(string)
    response_models     = optional(map(string))
  }))
  description = "Configuration of options method responses. Used to enable CORS policies."
  default     = {}
}

variable "options_integration_responses" {
  type = map(object({
    status_code         = string
    response_parameters = map(string)
  }))
  description = "Configuration of options method responses. Used to enable CORS policies."
  default     = {}
}
