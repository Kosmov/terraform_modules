data "aws_iam_policy_document" "this" {
  dynamic "statement" {
    for_each = var.statements

    content {
      sid     = statement.key
      effect  = statement.value.effect
      actions = statement.value.actions
      principals {
        type        = statement.value.principals.type
        identifiers = statement.value.principals.identifiers
      }
    }
  }
}

resource "aws_iam_role" "this" {
  name               = var.role_name
  assume_role_policy = data.aws_iam_policy_document.this.json
  # managed_policy_arns = var.managed_policy_arns
  permissions_boundary = var.permission_boundary
}

resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.this.name
  policy_arn = var.policy_arn
}
