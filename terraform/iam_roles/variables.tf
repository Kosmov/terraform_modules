variable "role_name" {
  type        = string
  description = "Role name."
}

variable "statements" {
  type = map(object({
    principals = object({
      type        = string
      identifiers = set(string)
    })
    effect  = string
    actions = set(string)
  }))
  description = "Trust relationship policy statements."
}

variable "policy_arn" {
  type        = string
  default     = null
  description = "Set of custom policy ARNs."
}

variable "permission_boundary" {
  type        = string
  default     = null
  description = "Permission boundry ARN."
}
