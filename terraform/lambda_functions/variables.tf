variable "filename" {
  type        = string
  description = "Lambda function file name."
}

variable "function_name" {
  type        = string
  description = "Lambda function name."
}

variable "role_arn" {
  type        = string
  description = "Role arn to associate with lambda function."
}

variable "handler" {
  type        = string
  description = "Lambda function handler."
}

variable "runtime" {
  type        = string
  description = "Lambda function runtime."
}

variable "environment_variables" {
  type        = map(string)
  description = "Lambda environment variables."
  default     = {}
}
