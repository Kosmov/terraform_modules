resource "aws_s3_bucket" "this" {
  bucket = var.bucket_name
}

resource "aws_s3_object" "this" {
  for_each     = var.upload_files ? fileset(var.path_to_files, "**") : null
  bucket       = aws_s3_bucket.this.id
  key          = each.value
  source       = "${var.path_to_files}/${each.value}"
  etag         = filemd5("${var.path_to_files}/${each.value}")
  content_type = lookup(local.mimes, regex("\\.[^.]+$", each.value), null)
}

resource "aws_s3_object" "local_content" {
  for_each     = var.upload_local_content ? var.local_content : null
  bucket       = aws_s3_bucket.this.id
  key          = each.key
  etag         = md5(each.value.content)
  content_type = each.value.content_type
  content      = each.value.content
}

resource "aws_s3_bucket_public_access_block" "this" {
  bucket = aws_s3_bucket.this.id

  block_public_acls       = var.access_configuration.block_public_acls
  block_public_policy     = var.access_configuration.block_public_policy
  ignore_public_acls      = var.access_configuration.ignore_public_acls
  restrict_public_buckets = var.access_configuration.restrict_public_buckets
}

resource "aws_s3_bucket_policy" "this" {
  count      = var.public_bucket ? 1 : 0
  depends_on = [aws_s3_bucket_public_access_block.this]

  bucket = aws_s3_bucket.this.id
  policy = data.aws_iam_policy_document.public_bucket.0.json
}

resource "aws_s3_bucket_website_configuration" "example" {
  count = var.public_bucket ? 1 : 0

  bucket = aws_s3_bucket.this.id
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "index.html"
  }
}
