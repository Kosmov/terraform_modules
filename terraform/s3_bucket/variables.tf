variable "bucket_name" {
  type        = string
  description = "Name of the s3 bucket."
}

variable "frontend_bucket" {
  type        = bool
  default     = false
  description = "Frontend bucket."
}

variable "upload_files" {
  type        = bool
  default     = false
  description = "Upload files for the bucket."
}

variable "path_to_files" {
  type        = string
  description = "Relative path to files that should be uploaded."
}

variable "public_bucket" {
  type        = bool
  description = "Bucket should be public."
}

variable "access_configuration" {
  type = object({
    block_public_acls       = bool
    block_public_policy     = bool
    ignore_public_acls      = bool
    restrict_public_buckets = bool
  })
  description = "Configure access policy for s3 bucket."
  default = {
    block_public_acls       = true
    block_public_policy     = true
    ignore_public_acls      = true
    restrict_public_buckets = true
  }
}

variable "upload_local_content" {
  type    = bool
  default = false
}

variable "local_content" {
  type = map(object({
    content_type = string
    content      = string
  }))
  default = null
}
