output "website_endpoint" {
  value = aws_s3_bucket.this.website_endpoint
}

output "hosted_zone_id" {
  value = aws_s3_bucket.this.hosted_zone_id
}

output "bucket_regional_domain_name" {
  value = aws_s3_bucket.this.bucket_regional_domain_name
}
