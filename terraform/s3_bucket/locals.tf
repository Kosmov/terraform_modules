locals {
  mimes = {
    ".png" : "image/png"
    ".json" : "application/json"
    ".ico" : "image/vnd.microsoft.icon"
    ".html" : "text/html"
    ".svg" : "image/svg+xml"
    ".txt" : "text/plain"
    ".map" : "application/json"
    ".js" : "text/javascript"
    ".css" : "text/css"
  }
}