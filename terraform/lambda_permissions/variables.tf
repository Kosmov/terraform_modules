variable "statement_id" {
  type        = string
  description = "Permission statement id."
}

variable "action" {
  type        = string
  description = "Permission action."
}

variable "function_name" {
  type        = string
  description = "Function name for which permission should be aplied"
}

variable "principal" {
  type        = string
  description = "Principal whcih which should be able to operate on lambda."
}

variable "source_arn" {
  type        = string
  description = "Source of the principal able to execute lambda function."
}
