variable "name" {
  type        = string
  description = "Name of the cognito pool."
}

variable "deletion_protection" {
  type        = string
  description = "Protection against accident deletion."
}

variable "minimum_length" {
  type        = number
  description = "Minimum password length."
}

variable "require_numbers" {
  type        = bool
  description = "Allow numbers in password."
}

variable "require_lowercase" {
  type        = bool
  description = "Allow lowercase letter in password."
}

variable "require_uppercase" {
  type        = bool
  description = "Allow uppercase letters in password."
}

variable "require_symbols" {
  type        = bool
  description = "Allow special characters in password."
}

variable "schemas" {
  type = set(object({
    data_type = string
    name      = string
    mutable   = bool
  }))
  description = "Set of cognito schemas."
  default     = []
}

variable "user_pool_clients" {
  type = map(object({
    generate_secret               = bool
    refresh_token_validity        = number
    access_token_validity         = number
    id_token_validity             = number
    explicit_auth_flows           = set(string)
    supported_identity_providers  = set(string)
    prevent_user_existence_errors = string
    access_token_validity_unit    = string
    id_token_validity_unit        = string
    refresh_token_validity_unit   = string
  }))
  description = "User pool app clients configuration."
  default     = {}
}
