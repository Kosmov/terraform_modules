resource "aws_cognito_user_pool" "this" {
  name                = var.name
  deletion_protection = var.deletion_protection
  password_policy {
    minimum_length    = var.minimum_length
    require_numbers   = var.require_numbers
    require_lowercase = var.require_lowercase
    require_uppercase = var.require_uppercase
    require_symbols   = var.require_symbols
  }

  dynamic "schema" {
    for_each = var.schemas

    content {
      attribute_data_type = schema.value.data_type
      name                = schema.value.name
      mutable             = schema.value.mutable
    }
  }

  lifecycle {
    ignore_changes = [schema, password_policy]
  }
}
resource "aws_cognito_user_pool_client" "example_client" {
  for_each = var.user_pool_clients

  name                          = each.key
  user_pool_id                  = aws_cognito_user_pool.this.id
  generate_secret               = each.value.generate_secret
  refresh_token_validity        = each.value.refresh_token_validity
  access_token_validity         = each.value.access_token_validity
  id_token_validity             = each.value.id_token_validity
  explicit_auth_flows           = each.value.explicit_auth_flows
  supported_identity_providers  = each.value.supported_identity_providers
  prevent_user_existence_errors = each.value.prevent_user_existence_errors

  token_validity_units {
    access_token  = each.value.access_token_validity_unit
    id_token      = each.value.id_token_validity_unit
    refresh_token = each.value.refresh_token_validity_unit
  }
}