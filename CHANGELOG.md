# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://github.com/mokkapps/changelog-generator-demo/compare/v1.0.0...v1.1.0) (2024-02-13)


### Features

* add scp module ([c841e4f](https://github.com/mokkapps/changelog-generator-demo/commits/c841e4f4cbc60cb273ef61966b60c0547547098a))

## [1.0.0](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.9...v1.0.0) (2024-02-12)

### [0.0.9](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.8...v0.0.9) (2024-02-09)


### Features

* add module for s3 buckets ([8ba215c](https://github.com/mokkapps/changelog-generator-demo/commits/8ba215ce354e22c3fd56127138d750ddb5beb125))

### [0.0.8](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.7...v0.0.8) (2024-02-09)


### Features

* add module for rest api ([493d002](https://github.com/mokkapps/changelog-generator-demo/commits/493d002c2d11cbc9fed619baa56bf0dd2d34b062))

### [0.0.7](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.6...v0.0.7) (2024-02-09)


### Features

* add module for lambda permissions ([251d55f](https://github.com/mokkapps/changelog-generator-demo/commits/251d55f9fa7973211190ae4734d7b3a14490bca7))

### [0.0.6](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.5...v0.0.6) (2024-02-09)


### Features

* add module for lambda functions ([313b3b6](https://github.com/mokkapps/changelog-generator-demo/commits/313b3b6d04cd5f38c53e737749515d3981db89f5))

### [0.0.5](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.4...v0.0.5) (2024-02-09)


### Features

* add module for iam roles ([89534b4](https://github.com/mokkapps/changelog-generator-demo/commits/89534b439ea72f2bdba44ce0e83ff66c70af996f))

### [0.0.4](https://github.com/mokkapps/changelog-generator-demo/compare/v0.0.3...v0.0.4) (2024-02-09)


### Features

* add modules for iam policies ([dd4be76](https://github.com/mokkapps/changelog-generator-demo/commits/dd4be76c2bd8ac9ce8fc02db4884dbaaab3aa04c))

### 0.0.3 (2024-02-09)


### Features

* add cognito module ([aaba1d4](https://github.com/mokkapps/changelog-generator-demo/commits/aaba1d4c30c3a7a41e0a6d48e523870872afe14b))
* add dynamodb_module ([62c7767](https://github.com/mokkapps/changelog-generator-demo/commits/62c77679168b9af1d78cf7db6d46d40e184c7467))
* configure precommit, and commit validation ([5558389](https://github.com/mokkapps/changelog-generator-demo/commits/55583891c40927a8e3c9e67ca629881a29823ebe))


### Bug Fixes

* set commit-msg file as executable ([9af6177](https://github.com/mokkapps/changelog-generator-demo/commits/9af61772da69b2baa3398e09d9c84dcbdf614f4a))

### 0.0.2 (2024-02-08)


### Features

* add cognito module ([aaba1d4](https://github.com/mokkapps/changelog-generator-demo/commits/aaba1d4c30c3a7a41e0a6d48e523870872afe14b))
* configure precommit, and commit validation ([5558389](https://github.com/mokkapps/changelog-generator-demo/commits/55583891c40927a8e3c9e67ca629881a29823ebe))


### Bug Fixes

* set commit-msg file as executable ([9af6177](https://github.com/mokkapps/changelog-generator-demo/commits/9af61772da69b2baa3398e09d9c84dcbdf614f4a))
